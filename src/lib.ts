import { IDictionary } from '@bettercorp/tools/lib/Interfaces';
import * as FS from 'fs';

export enum ITR069Events {
  DeviceAdded = "tr069-device-added",
  DeviceAdopted = "tr069-device-adopted",
  DeviceConnected = "tr069-device-connected",
  DeviceFileDownloaded = "tr069-device-file-downloaded",
  DeviceFileUploaded = "tr069-device-file-uploaded",

  DeviceInformed = "tr069-device-informed",
  DeviceActionAdded = "tr069-device-action-added",
  DeviceActionTriggered = "tr069-device-action-triggered",
  DeviceActionCompleted = "tr069-device-action-completed",

  FaultReceived = "tr069-fault-received"
}

export enum ITR069EAREvents {
  GetUploadFileUrl = "get-upload-file-url",
  GetFile = "get-file",
  AddFile = "add-file",
  AddFileStream = "add-file-stream",
  AddGroup = "add-group",
  AddUpdateGroup = "add-update-group",
  GetGroupConnection = "get-group-connection",
  GetDevice = "get-device",
  AdoptDevice = "adopt-device",
  RemoveDevice = "remove-device",
  AddAction = "add-action",
  GetActions = "get-actions",
  GetFaults = "get-faults",
  ClearFaults = "clear-faults",
}

export enum Tables {
  devices = "devices",
  actions = "actions",
  sessions = "sessions",
  groups = "groups",
  files = "files",
  faults = "faults",
  uploadedFiles = "uploadedFiles",
}

export interface DeviceInfo {
  serialNumber: string;
  manufacturer: string;
  productClass: string;
  oui: string;
}

const DeviceTableFields = ['id', 'groupId', 'lastConnectDate', 'lastConnectIP', 'synced', 'adopted', 'liveDebug', 'device', 'action', 'initAfter', 'data', 'tr069InformData'];
export function GetDeviceTableCleaned(data: Device): any {
  let dataToReturn: any = {};
  for (let field of DeviceTableFields) {
    dataToReturn[field] = (data as any)[field];
  }
  return dataToReturn;
}
export interface Device {
  id: string;
  device: DeviceInfo;
  deviceKey: string;
  groupId: string;
  lastConnectDate: number;
  lastConnectIP: string;
  synced: boolean;
  deleted: boolean;
  adopted: boolean;
  liveDebug: boolean;

  tr069InformData?: any;
  tr069LastInformTime: number;

  lastCredChange: number;
  cru: string;
  crp: string;
}

const DeviceActionTableFields = ['id', 'deviceId', 'createdTime', 'triggeredTime', 'completedTime', 'faultId', 'status', 'action', 'initAfter', 'data', 'extraMeta', 'groupId'];
export function GetDeviceActionTableCleaned(data: DeviceAction): any {
  let dataToReturn: any = {};
  for (let field of DeviceActionTableFields) {
    dataToReturn[field] = (data as any)[field];
  }
  return dataToReturn;
}
export interface IBCDeviceAction<T = any> extends IBCDeviceActionR<T> {
  responses: Array<any>;
}
export interface IBCDeviceActionR<T = any> {
  action: DeviceActionType;
  initAfter?: number;
  data?: T;
  extraMeta?: IDictionary<string>;
}
export interface DeviceAction<T = any> extends IBCDeviceAction<T> {
  id: string;
  deviceId: string;
  groupId: string;
  createdTime: number;
  triggeredTime: number | null;
  completedTime: number | null;
  faultId: string | null;
  status: DeviceActionStatus;
}
export enum DeviceActionType {
  reboot = 'reboot',
  downloadFile = 'downloadFile',
  downloadExternalFile = 'downloadExternalFile',
  uploadFile = 'uploadFile',
  setParameterValue = 'setParameterValue',
  getParameterValue = 'getParameterValue',
  getRPCMethods = 'getRPCMethods',
  specialAction = 'specialAction'
}
export enum DeviceActionStatus {
  ready = 'ready',
  triggered = 'triggered',
  completed = 'completed',
  faulted = 'faulted'
}
export interface Session {
  id: string;
  sessionkey: string;
  deviceId: string | null;
  groupId: string | null;
  time: number;
  envKeyType: string;
  trKey: string;
  device: DeviceInfo | null;
  tr069InformData?: any;
}
export interface Group {
  id: string;
  sourceId: string;
  sourcePlugin: string;
  urlKey: string;
  username: string;
  password: string;
  autoAdopt: boolean;
  manualAdopt: boolean;
}

export interface TR069EncryptedConnectionInfo {
  tc: number;
  sp: string;
  sgid: string;
}
export interface TR069ConnectionInfo {
  url: string;
  username: string;
  password: string;
}

export interface TRFile {
  id: string;
  note: string;
  added: number;
  hash: string;

  path: string;
  filename: string;
  fileType: TRFileType;
  fileSize: number;
  groupId?: string;
  deviceId?: string;
  extraMeta?: IDictionary<string>;
}
export enum TRFileType {
  firmwareUpgradeImage = "1 Firmware Upgrade Image",
  webContent = "2 Web Content",
  vendorConfigurationFile = "3 Vendor Configuration File",
  toneFile = "4 Tone File",
  ringerFile = "5 Ringer File",

  uploadVendorConfigurationFile = "1 Vendor Configuration File",
  uploadVendorLogFile = "2 Vendor Log File"
}
export interface TRFileResp extends TRFile {
  fileStream: FS.ReadStream;
}
export interface TRUploadedFileResp extends TRUploadedFile {
  fileStream: FS.ReadStream;
}

export interface TRUploadedFile {
  time: number;
  id: string;
  key: string;
  deviceId: string;
  groupId: string;
  sessionId: string;
  uploadType: TRFileType;
  store?: TRUploadedFileStored;
  extraMeta?: IDictionary<string>;
}
export interface TRUploadedFileStored {
  hash: string;
  path: string;
}

export interface Fault {
  id: string;
  time: number;
  session: Session,
  device: Device,
  body: any;
}

export enum TR069EARGetFileType {
  deviceUploadedFile = 'uFile',
  serverUploadedFile = 'file'
}
export interface TR069EARGetFile {
  filename: String;
  key: String;
  deviceId: string;
  type: TR069EARGetFileType,
  fileStreamId: string,
  id: string,
  groupId: string,
  tenantId: string,
}

export interface TR069EARAddFile {
  filename: string;
  groupId?: string;
  deviceId?: string;
  note?: string;
  type: TRFileType;
}

export interface TR069EARGetUploadFileUrl {
  deviceId: string;
  groupId: string;
  type: TRFileType;
}
export interface TR069EARGetUploadFileUrlResponse {
  url: string;
  username: string;
  password: string;
}
export interface XMLItem {
  name: string;
  value: string | number;
}
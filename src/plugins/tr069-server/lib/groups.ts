import { Tables, Group, TR069ConnectionInfo } from '../../../lib';
import { v4 as UUID } from 'uuid';
import { Tools } from '@bettercorp/tools/lib/Tools';
import * as crypto from 'crypto';
import { XTRA, Plugin } from '../plugin';
import { IDocumentSession } from 'ravendb/dist/Documents/Session/IDocumentSession';

export class LibGroups {
  private uSelf!: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }
  getSourcePluginName(id: string, session: IDocumentSession): Promise<string> {
    const self = this;
    return new Promise((resolve, reject) => {
      self.uSelf.log.debug(`GetGroup: ${ id }`);
      self.getGroups(id, session).then((x) => {
        if (Tools.isNullOrUndefined(x)) return reject('Unknown Group:getSourcePluginName');
        resolve((x as Group).sourcePlugin);
      }).catch(reject);
    });
  }
  getGroups(id: string, session: IDocumentSession): Promise<Array<Group> | Group> {
    //const self = this;
    return new Promise((resolve, reject): any => {
      if (Tools.isNullOrUndefined(id)) {
        return session.query<Group>({ collection: Tables.groups }).all().then(resolve).catch(reject);
      }
      session.query<Group>({ collection: Tables.groups }).whereEquals('id', id).single().then(resolve).catch(reject);
    });
  }
  getGroupBySource(sourceGroupId: String, sourcePlugin: string, session: IDocumentSession): Promise<Array<Group> | Group | null> {
    return session.query<Group>({ collection: Tables.groups }).whereEquals('sourcePlugin', sourcePlugin).whereEquals('sourceId', sourceGroupId).firstOrNull();
  }
  getConnectionDetails(sourceGroupId: string, sourcePlugin: string, session: IDocumentSession): Promise<TR069ConnectionInfo> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      let newGroup = await self.getGroupBySource(sourceGroupId, sourcePlugin, session) as Group;
      if (Tools.isNullOrUndefined(newGroup)) {
        return reject('Unknown group:getConnectionDetails');
      }
      resolve({
        url: `${ (await self.uSelf.getPluginConfig()).myHost }/LCS/A/${ newGroup.urlKey }`,
        username: newGroup.username,
        password: newGroup.password
      });
    });
  }
  getGroupByAuth(username: string, password: string, groupId: string, urlKey: string, session: IDocumentSession): Promise<Group | null> {
    return session.query<Group>({ collection: Tables.groups })
      .whereEquals('id', groupId)
      .whereEquals('urlKey', urlKey)
      .whereEquals('username', username)
      .whereEquals('password', password)
      .firstOrNull();
  }
  getGroupByAuthV2(username: string, password: string, urlKey: string, session: IDocumentSession): Promise<Group | null> {
    return session.query<Group>({ collection: Tables.groups })
      .whereEquals('urlKey', urlKey)
      .whereEquals('username', username)
      .whereEquals('password', password).firstOrNull();
  }
  createOrUpdateGroup(sourceGroupId: string, sourcePlugin: string, autoAdopt: boolean, session: IDocumentSession): Promise<string> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      let foundGroup = await self.getGroupBySource(sourceGroupId, sourcePlugin, session) as Group;
      self.uSelf.log.debug('createOrUpdateGroup', foundGroup)
      if (Tools.isNullOrUndefined(foundGroup)) {
        // new group
        return self.newGroup(sourceGroupId, sourcePlugin, autoAdopt, !autoAdopt, session).then((x) => resolve(x.id)).catch(reject);
      }
      foundGroup.autoAdopt = autoAdopt;
      foundGroup.manualAdopt = !autoAdopt;
      session.saveChanges().then(() => resolve(foundGroup.id)).catch(reject);
    });
  }
  async newGroup(sourceGroupId: string, sourcePlugin: string, autoAdopt: boolean, manualAdopt: boolean, session: IDocumentSession): Promise<Group> {
    if (autoAdopt == false && manualAdopt === false) throw 'Manual and Auto can`t both be false';
    if (autoAdopt == manualAdopt) throw 'Manual and Auto can`t both be the same';

    const newData = {
      id: UUID(),
      sourcePlugin,
      sourceId: sourceGroupId,
      urlKey: crypto.randomBytes(14).toString('hex'),
      username: crypto.randomBytes(15).toString('hex'),
      password: crypto.randomBytes(16).toString('hex'),
      autoAdopt,
      manualAdopt,
      "@metadata": {
        "@collection": Tables.groups
      }
    };

    await session.store(newData);
    await session.saveChanges();
    return newData;
  }
}

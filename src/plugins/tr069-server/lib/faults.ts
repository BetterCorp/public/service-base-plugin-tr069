import { Tables, Session, Device, ITR069Events, Fault } from '../../../lib';
import { v4 as UUID } from 'uuid';
import { XTRA, Plugin } from '../plugin';
import { IDocumentSession } from 'ravendb';

export class LibFaults {
  private uSelf!: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }

  clearFaults(docSession: IDocumentSession): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        let listOfFaults = await docSession.query<Fault>({ collection: Tables.faults }).selectFields<Fault>('id').all();
        for (let fault of listOfFaults)
          await docSession.delete(fault.id);
        await docSession.saveChanges();
        resolve();
      } catch (xc) {
        reject(xc);
      }
    });
  }

  getFaults(docSession: IDocumentSession): Promise<Array<Fault>> {
    return new Promise(async (resolve, reject) => {
      try {
        resolve(await docSession.query<Fault>({ collection: Tables.faults }).all());
      } catch (xc) {
        reject(xc);
      }
    });
  }

  newFault(session: Session, device: Device, body: any, docSession: IDocumentSession): Promise<void> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      const newFault = {
        id: UUID(),
        time: new Date().getTime(),
        session,
        device,
        body,
        "@metadata": {
          "@collection": Tables.faults
        }
      };
      try {
        await docSession.store(newFault);
        await docSession.saveChanges();
        await self.uSelf.emitEvent(null, ITR069Events.FaultReceived, newFault);
        resolve();
      } catch (xc) {
        reject(xc);
      }
    });
  }
}

import { Session, DeviceInfo } from '../../../lib';
import { v4 as UUID } from 'uuid';
import * as crypto from 'crypto';
import { XTRA, Plugin } from '../plugin';
import { Tools } from '@bettercorp/tools/lib/Tools';

export class LibSessions {
  private uSelf: Plugin;
  constructor(uSelf: Plugin, xtra: XTRA) {
    this.uSelf = uSelf;
  }

  async getSession(id: string): Promise<Session | null> {
    return JSON.parse(await this.uSelf.Redis.rejson_module_get(id, "."));
  }

  async newSession(envKeyType: string, info: DeviceInfo, trData: any): Promise<Session> {
    const newSession = {
      id: UUID(),
      sessionkey: crypto.randomBytes(14).toString('hex'),
      trKey: crypto.randomBytes(28).toString('hex'),
      deviceId: null,
      groupId: null,
      device: info,
      envKeyType: envKeyType,
      time: new Date().getTime(),
      tr069InformData: trData
    };
    await this.uSelf.Redis.rejson_module_set(newSession.id, ".", JSON.stringify(newSession));
    await this.uSelf.Redis.sendCommand({
      command: 'EXPIRE',
      args: [newSession.id, 300]
      // 5 minutes @ 1 second - sessions should never last more than 5 minutes. 
      // But incase the session isn't cleaned up, we auto expire the session.
    });
    return newSession;
  }

  async updateSession(id: string, deviceId: string, groupId: string): Promise<void> {
    if (!Tools.isNullOrUndefined(deviceId))
      await this.uSelf.Redis.rejson_module_set(`${ id }`, ".deviceId", `"${deviceId}"`);
    if (!Tools.isNullOrUndefined(groupId))
      await this.uSelf.Redis.rejson_module_set(`${ id }`, ".groupId", `"${groupId}"`);
  }

  async removeSession(id: string): Promise<void> {
    try {
      await this.uSelf.Redis.rejson_module_del(id, ".");
    } catch (e) { this.uSelf.log.error(e); } 
    // we don't really care about the error as the data will automatically be expired
  }
}

import { XTRA, Plugin } from '../plugin';
import { LibActions } from './actions';
import { LibDevices } from './devices';
import { LibFaults } from './faults';
import { LibFiles } from './files';
import { LibUFiles } from './filesu';
import { LibGroups } from './groups';
import { LibSessions } from './sessions';

export class DB {
  public Actions!: LibActions;
  public Devices!: LibDevices;
  public Faults!: LibFaults;
  public Files!: LibFiles;
  public UFiles!: LibUFiles;
  public Groups!: LibGroups;
  public Sessions!: LibSessions;

  constructor(uSelf: Plugin, xtra: XTRA) {
    this.Actions = new LibActions(uSelf, xtra);
    this.Devices = new LibDevices(uSelf, xtra);
    this.Faults = new LibFaults(uSelf, xtra);
    this.Files = new LibFiles(uSelf, xtra);
    this.UFiles = new LibUFiles(uSelf, xtra);
    this.Groups = new LibGroups(uSelf, xtra);
    this.Sessions = new LibSessions(uSelf, xtra);
  }

  async construct() {
    //await this.Actions.construct();
    //await this.Devices.construct();
    //await this.Faults.construct();
    //await this.Files.construct();
    //await this.UFiles.construct();
    //await this.Groups.construct();
    //await this.Sessions.construct();
  }
}
import { Tools } from "@bettercorp/tools/lib/Tools";
import { ITR069Events } from "../../lib";
import { XTRA, Plugin } from "./plugin";

export class Files {
  init(uSelf: Plugin, xtra: XTRA): Promise<void> {
    return new Promise((resolve) => {
      uSelf.fastify.get<
        any,
        {
          hash: string;
          filename: string;
        }
      >(
        "/LCF/:sessionId/:hash/:filename",
        async (req, res): Promise<any | void> => {
          const session = uSelf.RavenDB.openSession();
          try {
            let authorization: string | undefined = req.headers.authorization;
            if (Tools.isNullOrUndefined(authorization)) {
              uSelf.log.debug(":WWW-Auth");
              /*let enKEY = Object.keys(req.body)[0].split(':')[0];
            let serial = req.body[`${ enKEY }:Envelope`][`${ enKEY }:Body`][0]["cwmp:Inform"][0].DeviceId[0].SerialNumber[0];
            THIS_SESSION = await DB.addSession(enKEY, serial);
            res.setHeader('set-cookie', `session=${THIS_SESSION.id}`);*/
              res.header("WWW-Authenticate", 'Basic realm="401"');
              res.statusCode = 401;
              return res.send();
            }
            if (req.params === undefined) {
              res.statusCode = 400;
              return res.send();
            }
            let splData = Buffer.from(
              `${authorization}`.split(" ")[1],
              "base64"
            )
              .toString()
              .split(":");
            /*let device: Device | null;
          let sessionId = xtra.cleanString(req.params.sessionId);
          if (sessionId !== 'X')
            device = await DeviceLib.getDeviceFromSession(sessionId);*/
            let myFile = await uSelf.DB.Files.getFile(
              splData[0],
              xtra.cleanString(req.params.hash),
              xtra.cleanString(req.params.filename),
              session
            );
            res.header("content-type", "application/octet-stream");
            res.send(myFile.fileStream)
            //myFile.fileStream.pipe(res);
            await uSelf.emitEvent(null, ITR069Events.DeviceFileDownloaded, {
              time: new Date().getTime(),
              ip: req.socket.remoteAddress,
              added: myFile.added,
              fileType: myFile.fileType,
              filename: myFile.filename,
              groupId: myFile.groupId,
              deviceId: myFile.deviceId,
              note: myFile.note,
              hash: myFile.hash,
              fileSize: myFile.fileSize,
              path: myFile.path,
            });
          } catch (exc) {
            uSelf.log.error(exc);
            res.statusCode = 400;
            //res.sendStatus(400);
            return res.send();
          }
        }
      );
      /*uSelf.express.get('/LDF/:fileId/:hash/:filename', async (req: ERequest, res: EResponse): Promise<any | void> => {
        const session = uSelf.RavenDB.openSession();
        try {
          let myFile = await uSelf.DB.Files.getFile(xtra.cleanString(req.params.fileId), xtra.cleanString(req.params.hash), xtra.cleanString(req.params.filename), session);
          res.setHeader("content-type", "application/octet-stream");
          myFile.fileStream.pipe(res);
        } catch (exc) {
          uSelf.log.error(exc);
          res.sendStatus(400);
        }
      });*/
      resolve();
    });
  }
}

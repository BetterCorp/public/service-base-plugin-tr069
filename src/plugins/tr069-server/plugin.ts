import { CPlugin } from '@bettercorp/service-base/lib/interfaces/plugins';
//import { Tools } from '@bettercorp/tools/lib/Tools';
import { Server } from './server';
import { Inform } from './inform';
import { Files } from './files';
import { UFiles } from './filesu';
import { Events } from './events';
import { IPluginConfig, IPluginConfigStorage } from './sec.config';
import { fastify } from '@bettercorp/service-base-plugin-web-server/lib/plugins/fastify/fastify';
//import * as redis from 'ioredis';
import { DocumentStore } from "ravendb";
import { Redis } from 'redis-modules-sdk';
import { DB } from './lib/db';
import * as FS from 'fs';
import * as PATH from 'path';
import { Demo } from './demo';
import { Tools } from '@bettercorp/tools/lib/Tools';
const Minio = require('minio');

const inits: any = {
  Server,
  Files,
  UFiles,
  Inform,
  Events,
  Demo
};

export interface XTRA {
  cleanRegex: RegExp;
  infoCleanRegex: RegExp;
  ipCleanRegex: RegExp;
  maxLimit: number;
  cleanString(x: string): string;
}

/*export class tr069 extends CPluginClient<IPluginConfig> {
  public readonly _pluginName: string = "tr069-server";  
  private MinioClient: any;

  constructor(self: CPlugin) {
    super(self);
    if (await this.getPluginConfig().storage === IPluginConfigStorage.MINIO) {
      this.MinioClient = new Minio.Client(self.getPluginConfig(this._pluginName).minio);
    }
  }
}*/

export class Plugin extends CPlugin<IPluginConfig> {
  fastify!: fastify;
  //private DB!: Database;
  public S3!: any;
  public RavenDB!: DocumentStore;
  public Redis!: Redis;
  public DB!: DB;
  public getMetaField(objet: any, field: string, defalt: string | null | undefined) {
    if (Tools.isNullOrUndefined(objet.meta)) return defalt;
    if (Tools.isNullOrUndefined(objet.meta[field])) return defalt;
    return objet.meta[field];
  }
  init(): Promise<void> {
    let cleanRegex = /(?![,-:~])[\W]/g;
    let infoCleanRegex = /(?![,-:~ +_.@])[\W]/g;
    let ipCleanRegex = /(?![.0-9])[\W]/g;
    const self = this;
    return new Promise(async (resolve, reject) => {
      self.fastify = new fastify(self);
      (await self.fastify.getServerInstance()).addContentTypeParser('*', (req: any, done: any) => {
        done();
      });

      try {
        const storMech = (await this.getPluginConfig()).storage;
        if (storMech === IPluginConfigStorage.LOCAL) {
          const localDir = (await this.getPluginConfig()).localIsFullPath ? (await this.getPluginConfig()).local : PATH.join(this.cwd, (await this.getPluginConfig()).local);
          if (!FS.existsSync(localDir))
            FS.mkdirSync(localDir);
        } else if (storMech === IPluginConfigStorage.S3) {
          this.S3 = new Minio.Client((await this.getPluginConfig()).S3);
        } else return reject(`NO SUPPORT FOR ${ storMech } STORAGE YET`);

        self.log.info(`Setup DB[RAVEN] ${ (await self.getPluginConfig()).db.raven.url }/${ (await self.getPluginConfig()).db.raven.databaseName }`);
        self.RavenDB = new DocumentStore((await self.getPluginConfig()).db.raven.url, (await self.getPluginConfig()).db.raven.databaseName, (await self.getPluginConfig()).db.raven.auth!);
        self.log.info(`Connect DB[RAVEN] ${ (await self.getPluginConfig()).db.raven.url }/${ (await self.getPluginConfig()).db.raven.databaseName }`);
        await self.RavenDB.initialize();
        self.log.info(`Ready DB[RAVEN]`);
        self.log.info(`Setup DB[REDIS] ${ (await self.getPluginConfig()).db.redis.host }/${ (await self.getPluginConfig()).db.redis.db }`);
        self.Redis = new Redis((await self.getPluginConfig()).db.redis);
        self.log.info(`Connect DB[REDIS] ${ (await self.getPluginConfig()).db.redis.host }/${ (await self.getPluginConfig()).db.redis.db }`);
        await self.Redis.connect();
        self.log.info(`Ready DB[REDIS]`);
      } catch (exce) {
        self.log.fatal(exce);
        return reject(exce);
      }
      //self.DocStore.
      /*self.DB = new Database((await self.getPluginConfig()).db);
      if (!Tools.isNullOrUndefined((await self.getPluginConfig()).db.auth)) {
        self.DB.useBasicAuth((await self.getPluginConfig()).db.auth!.username, (await self.getPluginConfig()).db.auth!.password);
      }*/

      const xtra: XTRA = {
        cleanRegex: cleanRegex,
        infoCleanRegex: infoCleanRegex,
        ipCleanRegex,
        maxLimit: 255,
        cleanString: (x) => `${ x }`.trim().replace(cleanRegex, '').trim().substring(0, 255)
      };

      self.DB = new DB(self, xtra);

      for (let initName of Object.keys(inits)) {
        self.log.info(`Load Internal Plugin: ${ initName }`);
        new inits[initName]().init(self, xtra);
      }
      resolve();
    });
  }
}
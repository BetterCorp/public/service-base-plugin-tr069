import { XTRA, Plugin } from "./plugin";
//const bodyParser = require('body-parser');
//require('body-parser-xml')(bodyParser);

export class Server {
  init(uSelf: Plugin, xtra: XTRA): Promise<void> {
    return new Promise((resolve) => {
      uSelf.log.info("USE JSON FOR WEB");
      /*uSelf.express.use(bodyParser.xml({
        limit: '15mb'
      }));*/
      uSelf.fastify.register(require("fastify-xml-body-parser"));
      resolve();
    });
  }
}

import { Tools } from "@bettercorp/tools/lib/Tools";
//import { Request as ERequest, Response as EResponse } from "express";
import { Device, DeviceAction, ITR069Events, Session } from "../../lib";
import { DSC } from "./deviceSpecificConfig";
import { XTRA, Plugin } from "./plugin";

export class Inform {
  async init(uSelf: Plugin, xtra: XTRA): Promise<void> {
    return new Promise((resolve) => {
      uSelf.fastify.post<
        any,
        {
          extraEncrypt: string;
          groupId: string;
        }
      >(
        "/LCS/:groupId/:extraEncrypt",
        //async (req: ERequest, res: EResponse): Promise<any | void> => {
        async (req, res): Promise<any | void> => {
          if (req.params === undefined) {
            res.statusCode = 400;
            return res.send();
          }
          /*console.log(req.body, typeof req.body);

          let enKEY1 = xtra.cleanString(Object.keys(req.body)[0].split(":")[0]);
          console.log(enKEY1);
          console.log(req.body[`${enKEY1}:Envelope`]);
          console.log(req.body[`${enKEY1}:Envelope`][`${enKEY1}:Body`]);
          console.log(
            req.body[`${enKEY1}:Envelope`][`${enKEY1}:Body`]["cwmp:Inform"]
          );*/
          /*req.body[`${enKEY1}:Envelope`][`${enKEY1}:Body`][0][
            "cwmp:Inform"
          ][0].DeviceId[0].SerialNumber[0]*/
          //          res.statusCode = 201;
          //return res.send();
          const session = uSelf.RavenDB.openSession();
          uSelf.log.info(
            `RE:${req.headers.cookie}:${req.headers.authorization}`,
            req.headers["ip"]
          );
          uSelf.log.debug(JSON.stringify(req.body));
          let cookie: string = req.headers.cookie!;
          let authorization: string = req.headers.authorization!;
          let THIS_SESSION: Session | null;
          let THIS_DEVICE: Device | null;

          if (!Tools.isNullOrUndefined(cookie)) {
            try {
              let actCookie = cookie.split("=")[1];
              uSelf.log.debug("FIND SESSION: " + actCookie);
              THIS_SESSION = await uSelf.DB.Sessions.getSession(
                xtra.cleanString(actCookie)
              );
              if (
                !Tools.isNullOrUndefined(THIS_SESSION) &&
                !Tools.isNullOrUndefined(THIS_SESSION!.deviceId)
              ) {
                THIS_DEVICE = await uSelf.DB.Devices.getDevice(
                  THIS_SESSION!.deviceId!,
                  session
                );
                if (Tools.isNullOrUndefined(THIS_DEVICE)) {
                  res.statusCode = 400;
                  return res.send();
                }
                if (THIS_DEVICE!.deleted) {
                  res.statusCode = 400;
                  return res.send();
                }
              }
              /*let enKEY = Object.keys(req.body)[0].split(':')[0];
            let serial = req.body[`${ enKEY }:Envelope`][`${ enKEY }:Body`][0]["cwmp:Inform"][0].DeviceId[0].SerialNumber[0];
            let newSession = await DB.addSession(enKEY, serial);
            res.setHeader('set-cookie', newSession.id);
            res.setHeader('WWW-Authenticate', 'Basic realm="401"');
            features.log.info(`::401:A`);
            return res.sendStatus(401);*/
            } catch (exc) {
              uSelf.log.error(exc);
              await uSelf.DB.Faults.newFault(
                THIS_SESSION!,
                THIS_DEVICE!,
                {
                  body: req.body,
                  err: exc,
                  note: "FAILED TO SET SESSION",
                },
                session
              );
              res.statusCode = 400;
              return res.send();
            }
          } else if (Tools.isNullOrUndefined(authorization)) {
            uSelf.log.debug(":WWW-Auth");
            /*let enKEY = Object.keys(req.body)[0].split(':')[0];
          let serial = req.body[`${ enKEY }:Envelope`][`${ enKEY }:Body`][0]["cwmp:Inform"][0].DeviceId[0].SerialNumber[0];
          THIS_SESSION = await DB.addSession(enKEY, serial);
          res.setHeader('set-cookie', `session=${THIS_SESSION.id}`);*/
            res.header("WWW-Authenticate", 'Basic realm="401"');
            res.statusCode = 401;
            return res.send();
          }
          // console.log(`RE:${req.headers.cookie}:${req.headers.authorization}`)
          // if (Tools.isNullOrUndefined(THIS_SESSION))
          //   return res.sendStatus(400);

          // res.setHeader('set-cookie', THIS_SESSION.id);

          if (
            Tools.isNullOrUndefined(THIS_SESSION!) &&
            !Tools.isNullOrUndefined(authorization)
          ) {
            let logs: Array<any> = [];
            const logDebug = (...args: Array<any>) => {
              args = logs.concat(args);
              uSelf.log.debug(args);
            };
            try {
              if (Tools.isNullOrUndefined(req.body) || `${req.body}` == "")
                throw "UNKNOWN BODY";

              let enKEY = xtra.cleanString(
                Object.keys(req.body)[0].split(":")[0]
              );
              let serial = xtra.cleanString(
                req.body[`${enKEY}:Envelope`][`${enKEY}:Body`]["cwmp:Inform"]
                  .DeviceId.SerialNumber
              );
              let Manufacturer = xtra.cleanString(
                req.body[`${enKEY}:Envelope`][`${enKEY}:Body`]["cwmp:Inform"]
                  .DeviceId.Manufacturer
              );
              let OUI = xtra.cleanString(
                req.body[`${enKEY}:Envelope`][`${enKEY}:Body`]["cwmp:Inform"]
                  .DeviceId.OUI
              );
              let ProductClass = xtra.cleanString(
                req.body[`${enKEY}:Envelope`][`${enKEY}:Body`]["cwmp:Inform"]
                  .DeviceId.ProductClass
              );
              logDebug(`NEW SESH`, {
                serialNumber: serial,
                manufacturer: Manufacturer,
                oui: OUI,
                productClass: ProductClass,
              });
              THIS_SESSION = await uSelf.DB.Sessions.newSession(
                enKEY,
                {
                  serialNumber: serial,
                  manufacturer: Manufacturer,
                  oui: OUI,
                  productClass: ProductClass,
                },
                req.body
              );
              logDebug(req.body[`${enKEY}:Envelope`][`${enKEY}:Body`]);
              res.header("set-cookie", `session=${THIS_SESSION.id}`);
              res.header("Set-Cookie", `session=${THIS_SESSION.id}`);
              logDebug(
                `SET SESSION: ${THIS_SESSION.id} : `,
                THIS_SESSION,
                THIS_SESSION.device
              );

              let splData = Buffer.from(authorization.split(" ")[1], "base64")
                .toString()
                .split(":");
              logDebug("splData");
              let usrname = xtra.cleanString(splData[0]);
              let passwd = xtra.cleanString(splData[1]);
              let extraE = xtra.cleanString(req.params.extraEncrypt);
              logDebug({
                username: usrname,
                password: passwd,
                extra: extraE,
              });
              let cleanGroupId = xtra.cleanString(req.params.groupId);
              let device =
                cleanGroupId === "A"
                  ? await uSelf.DB.Devices.getDeviceFromAuthV3(
                      usrname,
                      passwd,
                      extraE,
                      THIS_SESSION!.device!,
                      (req as any).headers["ip"],
                      session
                    )
                  : await uSelf.DB.Devices.getDeviceFromAuth(
                      usrname,
                      passwd,
                      extraE,
                      cleanGroupId,
                      THIS_SESSION!.device!,
                      (req as any).headers["ip"],
                      session
                    );

              logDebug("inform:getDeviceFromAuth:isNullOrUndefined", device);

              if (Tools.isNullOrUndefined(device)) {
                uSelf.log.info(`::400:D`);
                await uSelf.DB.Faults.newFault(
                  THIS_SESSION,
                  THIS_DEVICE!,
                  {
                    body: req.body,
                    note: "INVALID AUTHORIZATION",
                  },
                  session
                );
                res.statusCode = 400;
                return res.send();
              }
              logDebug(
                "inform:getDeviceFromAuth:updateSession",
                THIS_SESSION,
                device
              );
              await uSelf.DB.Sessions.updateSession(
                THIS_SESSION.id,
                device!.id,
                device!.groupId
              );

              logDebug("inform:req.body", req.body);
              if (Object.keys(req.body).length === 0) {
                uSelf.log.info(`::204`);
                res.statusCode = 204;
                return res.send();
              }
              uSelf.log.info(`::200`);
              return res.send(`<?xml version="1.0" encoding="UTF-8"?>
                <${THIS_SESSION.envKeyType}:Envelope xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:${THIS_SESSION.envKeyType}="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cwmp="urn:dslforum-org:cwmp-1-0">
                    <${THIS_SESSION.envKeyType}:Header>
                        <cwmp:ID ${THIS_SESSION.envKeyType}:mustUnderstand="1">${THIS_SESSION.trKey}</cwmp:ID>
                    </${THIS_SESSION.envKeyType}:Header>
                    <${THIS_SESSION.envKeyType}:Body>
                        <cwmp:InformResponse>
                            <MaxEnvelopes>1</MaxEnvelopes>
                        </cwmp:InformResponse>
                    </${THIS_SESSION.envKeyType}:Body>
                </${THIS_SESSION.envKeyType}:Envelope>`);
            } catch (exc) {
              uSelf.log.error(exc);
              await uSelf.DB.Faults.newFault(
                THIS_SESSION!,
                THIS_DEVICE!,
                {
                  body: req.body,
                  err: exc,
                  logs,
                  note: "ERROR DURING AUTHORIZATION",
                },
                session
              );
              res.statusCode = 400;
              return res.send();
            }
          }
          uSelf.log.info(`::EEE`);
          if (Tools.isNullOrUndefined(THIS_SESSION!)) {
            await uSelf.DB.Faults.newFault(
              THIS_SESSION!,
              THIS_DEVICE!,
              {
                body: req.body,
                note: "INVALID SESSION",
              },
              session
            );
            res.statusCode = 400;
            return res.send();
          }
          if (Tools.isNullOrUndefined(THIS_SESSION!.deviceId)) {
            uSelf.log.info(`::400`);
            await uSelf.DB.Faults.newFault(
              THIS_SESSION!,
              THIS_DEVICE!,
              {
                body: req.body,
                note: "INVALID DEVICE",
              },
              session
            );
            res.statusCode = 400;
            return res.send();
          }

          if (THIS_DEVICE!.deleted) {
            uSelf.log.info(`::DELETD:E`);
            await uSelf.DB.Faults.newFault(
              THIS_SESSION!,
              THIS_DEVICE!,
              {
                body: req.body,
                note: "DEVICE DELETED, CANNOT DO ANYTHING FOR IT",
              },
              session
            );
            res.statusCode = 400;
            return res.send();
          }

          if (
            new Date().getTime() -
              1000 *
                60 *
                (await uSelf.getPluginConfig()).triggerCredResetTimeMinutes >
            THIS_DEVICE!.lastCredChange
          ) {
            THIS_DEVICE = await uSelf.DB.Devices.credUpdate(
              THIS_DEVICE!.id,
              session
            );
            let sendData = DSC.SetDeviceConnectionDetails(
              THIS_DEVICE.cru,
              THIS_DEVICE.crp,
              !THIS_DEVICE!.adopted || THIS_DEVICE.liveDebug
                ? (await uSelf.getPluginConfig()).checkinTimeDebug
                : (await uSelf.getPluginConfig()).checkinTime,
              THIS_DEVICE.device,
              THIS_SESSION!
            );
            uSelf.log.debug(sendData);
            return res.send(sendData);
          }

          if (!THIS_DEVICE!.adopted) {
            uSelf.log.info(`::204:E`);
            res.statusCode = 204;
            return res.send();
          }

          try {
            if (
              !Tools.isNullOrUndefined(
                req.body[`${THIS_SESSION!.envKeyType}:Envelope`][
                  `${THIS_SESSION!.envKeyType}:Body`
                ][`${THIS_SESSION!.envKeyType}:Fault`]
              )
            ) {
              await uSelf.DB.Faults.newFault(
                THIS_SESSION!,
                THIS_DEVICE!,
                {
                  body: req.body,
                  fault:
                    req.body[`${THIS_SESSION!.envKeyType}:Envelope`][
                      `${THIS_SESSION!.envKeyType}:Body`
                    ][`${THIS_SESSION!.envKeyType}:Fault`],
                  note: "TR06-FAULT REQ",
                },
                session
              );
              res.statusCode = 204;
              return res.send();
            }
          } catch (erx) {}
          uSelf.log.debug(
            "inform:getAvailableDeviceAction",
            THIS_SESSION!,
            req.body
          );
          let deviceAction: DeviceAction | null =
            await uSelf.DB.Actions.getAvailableDeviceAction(
              THIS_SESSION!,
              req.body,
              session
            );
          if (!Tools.isNullOrUndefined(deviceAction)) {
            uSelf.log.debug("RECEIVED:", JSON.stringify(req.body));
            /*if (dRunID === THIS_DEVICE.id) {
            dRunCount++;
            if (dRunCount > 2) return res.sendStatus(400);
          }*/
            //dRunID = THIS_DEVICE.id;
            //await DB.rebootDeviceSent(THIS_DEVICE.id);
            uSelf.log.debug("deviceAction", JSON.stringify(deviceAction));
            const sendAction = `<?xml version="1.0" encoding="UTF-8"?>
                <${
                  THIS_SESSION!.envKeyType
                }:Envelope xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:${
              THIS_SESSION!.envKeyType
            }="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cwmp="urn:dslforum-org:cwmp-1-0">
                    <${THIS_SESSION!.envKeyType}:Header>
                        <cwmp:ID ${
                          THIS_SESSION!.envKeyType
                        }:mustUnderstand="1">${THIS_SESSION!.trKey}</cwmp:ID>
                    </${THIS_SESSION!.envKeyType}:Header>
                    <${THIS_SESSION!.envKeyType}:Body>
                      ${
                        /*deviceAction.action === DeviceActionType.specialAction
                          ? deviceAction.data.xml
                          : */
                        await uSelf.DB.Actions.getActionBody(
                          deviceAction.action,
                          deviceAction.data,
                          (
                            await uSelf.getPluginConfig()
                          ).myHost,
                          THIS_SESSION!,
                          session
                        )
                      }
                    </${THIS_SESSION!.envKeyType}:Body>
                </${THIS_SESSION!.envKeyType}:Envelope>`;
            uSelf.log.debug("SEND", sendAction);
            return res.send(sendAction);
          }

          uSelf.log.info(`::204:E`);
          try {
            THIS_DEVICE!.lastConnectDate = new Date().getTime();
            THIS_DEVICE!.lastConnectIP = req.headers["ip"] as string;
            let informData = {
              device: {
                manufacturer:
                  THIS_SESSION!.tr069InformData[
                    `${THIS_SESSION!.envKeyType}:Envelope`
                  ][`${THIS_SESSION!.envKeyType}:Body`][`cwmp:Inform`].DeviceId
                    .Manufacturer,
                oui: THIS_SESSION!.tr069InformData[
                  `${THIS_SESSION!.envKeyType}:Envelope`
                ][`${THIS_SESSION!.envKeyType}:Body`][`cwmp:Inform`].DeviceId
                  .OUI,
                productClass:
                  THIS_SESSION!.tr069InformData[
                    `${THIS_SESSION!.envKeyType}:Envelope`
                  ][`${THIS_SESSION!.envKeyType}:Body`][`cwmp:Inform`].DeviceId
                    .ProductClass,
                serialNumber:
                  THIS_SESSION!.tr069InformData[
                    `${THIS_SESSION!.envKeyType}:Envelope`
                  ][`${THIS_SESSION!.envKeyType}:Body`][`cwmp:Inform`].DeviceId
                    .SerialNumber,
              },
              parameterList:
                THIS_SESSION!.tr069InformData[
                  `${THIS_SESSION!.envKeyType}:Envelope`
                ][`${THIS_SESSION!.envKeyType}:Body`][`cwmp:Inform`]
                  .ParameterList,
            };
            THIS_DEVICE!.tr069InformData = informData;
            (THIS_DEVICE!.tr069LastInformTime =
              new Date().getTime() - THIS_SESSION!.time),
              await uSelf.DB.Devices.updateDevice(
                (THIS_DEVICE! as Device).id,
                THIS_DEVICE! as Device,
                session
              );
          } catch (exc) {
            await uSelf.DB.Faults.newFault(
              THIS_SESSION!,
              THIS_DEVICE!,
              {
                body: req.body,
                err: exc,
                note: "FAILED TO SESSION INFORM",
              },
              session
            );
            uSelf.log.error(exc);
          }
          await uSelf.emitEvent(null, ITR069Events.DeviceInformed, {
            deviceId: THIS_DEVICE!.id,
            groupId: THIS_SESSION!.groupId,
            time: new Date().getTime(),
            tr069LastInformTime: new Date().getTime() - THIS_SESSION!.time,
            ip: req.headers["ip"],
          });
          await uSelf.DB.Sessions.removeSession(THIS_SESSION!.id);
          res.statusCode = 204;
          return res.send();
        }
      );
      resolve();
    });
  }
}

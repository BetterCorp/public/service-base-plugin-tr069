import * as crypto from 'crypto';
import { RedisOptions } from 'ioredis';
import { IAuthOptions } from 'ravendb';

export interface IPluginConfig {
  secureKey: string;
  checkinTime: number;
  checkinTimeDebug: number;
  triggerCredResetTimeMinutes: number;
  myHost: string;
  db: IPluginConfigDB;
  storage: IPluginConfigStorage;
  S3: IPluginConfigMinio;
  //backblaze: IPluginConfigBlackblaze;
  local: string;
  localIsFullPath: boolean;
  //runSessionsCleanup: boolean;
}
export enum IPluginConfigStorage {
  S3 = 'S3',
  LOCAL = 'local'
}
export interface IPluginConfigBlackblaze {
  bucket: string;
  keyId: string;
  appKey: string;
}
export interface IPluginConfigMinio {
  endPoint: string;
  port: number;
  useSSL: boolean;
  accessKey: string;
  secretKey: string;
  bucket: string;
  path: string;
}

export interface IPluginConfigDB {
  redis: RedisOptions;
  raven: IPluginConfigDBRaven;
}
export interface IPluginConfigDBRaven {
  url: string;
  databaseName: string;
  auth?: IAuthOptions;
}

export default (): IPluginConfig => {
  return {
    db: {
      redis: {
        host: '127.0.0.1',
        port: 6379,
        db: 0
      },
      raven: {
        url: 'http://localhost:8080',
        databaseName: 'testdb'
      }
    },
    secureKey: crypto.randomBytes(65).toString('hex'),
    checkinTime: 300,
    checkinTimeDebug: 10,
    triggerCredResetTimeMinutes: 4,
    //runSessionsCleanup: true,
    myHost: 'http://localhost:7547',
    storage: IPluginConfigStorage.LOCAL,
    local: './data',
    localIsFullPath: false,
    /*backblaze: {
      bucket: '',
      keyId: '',
      appKey: ''
    },*/
    S3: {
      endPoint: '127.0.0.1',
      port: 80,
      useSSL: true,
      accessKey: 'admin',
      secretKey: 'password',
      bucket: 'myBucket',
      path: '/'
    },
  };
};